<?php
session_start();

require "vendor/autoload.php";

$app = new \Slim\Slim();
\config\Connexion::connection("config.ini");

/*
 * TODO :
 * - Filtrer par département
 * - Supprimer photos
 * - Optimiser l'édition et l'ajout en fusionnant les controleurs
 * - Modifier la fonction de génération de Token pour ne pas faire de $_SESSION['token'] = $token à l'intérieur de la fonction
 */

/*-----------------------*\
         Index
\*-----------------------*/

$app->get('/', function() use ($app) {
    $c = new \controller\IndexController($app->request);
    $c->index();
})->name('index');


/*-----------------------*\
         Annonce
\*-----------------------*/

$app->get('/annonces/', function() use ($app) {
    $c = new \controller\AnnonceController($app->request);
    $c->liste();
})->name('annonces');

$app->group('/annonce', function() use ($app) {
    $app->get('/:id',function($id) use($app) {
        $c = new \controller\AnnonceController($app->request);
        $c->detail($id);
    })->name('annonce');

    $app->get('/:id/editer', function($id) use ($app) {
        $c = new \controller\AnnonceController($app->request);
        $c->edition($id);
    })->name('editerAnnonce');

    $app->post('/:id/editer', function ($id) use ($app) {
        $c = new \controller\AnnonceController($app->request);
        $c->editionPost($id);
    })->name('editerAnnoncePost');

    $app->post('/:id/supprimer',function($id) use($app) {
        $c = new \controller\AnnonceController($app->request);
        $c->supprimer($id);
    })->name('supprimerAnnonce');
});



$app->get('/recherche',function () use ($app){
    $c = new \controller\AnnonceController($app->request);
    $c->recherche();
})->name('recherche');

$app->get('/ajouter', function() use ($app) {
    $c = new \controller\AnnonceController($app->request);
    $c->ajouter();
})->name('ajouterAnnonce');


$app->post('/ajouter', function() use ($app) {
    $c = new \controller\AnnonceController($app->request);
    $c->ajouterPost();
})->name('ajouterAnnoncePost');




/*-----------------------*\
         Erreur
\*-----------------------*/

$app->error(function ($e) use ($app) {
    if(is_string($e)) {
        $app->flash('error', $e);
        $app->redirect($app->urlFor('index'));
    }

    $app->flash('error', $e['message']);
    if(!array_key_exists('route', $e)) {
        $e['route'] = 'index';
    }
    if(array_key_exists('param', $e)) {
        $app->redirect($app->urlFor($e['route'], $e['param']));
    }
    $app->redirect($app->urlFor($e['route']));
});


/*-----------------------*\
         Catégorie
\*-----------------------*/

$app->get('/categorie/:id', function($id) use ($app) {
    $c = new \controller\CategorieController($app->request);
    $c->annonces($id);
})->name('categorie');


/*-----------------------*\
         Connexion / Inscription / Deconnexion
\*-----------------------*/

$app->get('/connexion', function() use ($app) {
    $c = new \controller\MembreController($app->request);
    $c->connexion();
})->name('connexion');

$app->post('/connexion', function() use ($app) {
    $c = new \controller\MembreController($app->request);
    $c->connexionPost();
})->name('connexionPost');

$app->get('/inscription', function() use ($app) {
    $c = new \controller\MembreController($app->request);
    $c->inscription();
})->name('inscription');

$app->post('/inscription', function() use ($app) {
    $c = new \controller\MembreController($app->request);
    $c->inscriptionPost();
})->name('inscriptionPost');

$app->get('/deconnexion', function() use ($app) {
    $c = new \controller\MembreController($app->request);
    $c->deconnexion();
})->name('deconnexion');


$app->group('/profil', function() use ($app) {
    $app->get('', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->profil();
    })->name('profil');

    $app->get('/editer', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->editer();
    })->name('editerProfil');

    $app->post('/editer', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->editerPost();
    })->name('editerProfilPost');

    $app->get('/mot-de-passe', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->motDePasse();
    })->name('editerMotDePasse');

    $app->post('/mot-de-passe', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->motDePassePost();
    })->name('editerMotDePassePost');

    $app->get('/api', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->api();
    })->name('genererAPI');

    $app->post('/api', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->apiPost();
    })->name('genererAPIPost');

    $app->get('/mesAnnonces/', function() use ($app) {
        $c = new \controller\MembreController($app->request);
        $c->mesAnnonces();
    })->name('mesAnnonces');
});


/*-----------------------*\
         API
\*-----------------------*/

$app->group('/api',function() use ($app) {

    $app->group('/annonces',function() use ($app){
        $app->get('',function() use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;

            try {
                $annonces = \model\Annonce::all();
                if (sizeof($annonces) == 0) {
                    $app->response->setStatus(500);
                    $response['status'] = 500;
                }else{
                    foreach($annonces as $annonce){
                        $categorie = \model\Categorie::find($annonce->id_categorie);
                        $categorie->type = "categorie";
                        $img = $categorie->image;
                        $categorie->image = array("href"=>$img);
                        $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                        $link = array("self"=>$self);
                        $categorie->link = $link;

                        $annonce->type = "annonce";
                        unset($annonce->id_categorie);
                        $annonce->categorie = $categorie;
                        $self = array("href"=>$app->urlFor("api/annonces/id",array('id'=>$annonce->id)));
                        $link=array("self"=>$self);
                        $annonce->link = $link;
                    }
                    $response['results'] = $annonces;
                }
            }catch(Exception $e){
                $response['status'] = 500;
                $app->response->setBody("");
                $app->response->setStatus(500);
            }
            echo json_encode($response);
        })->name("api/annonces");

        $app->get('/:id',function($id) use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;
            try {
                $annonce = \model\Annonce::find($id);
                if ($annonce == null) {
                    $app->response->setStatus(404);
                    $response['status'] = 404;
                } else {
                    $categorie = \model\Categorie::find($annonce->id_categorie);
                    $categorie->type = "categorie";
                    $img = $categorie->image;
                    $categorie->image = array("href"=>$img);
                    $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                    $link = array("self"=>$self);
                    $categorie->link = $link;
                    $annonce->type = "annonce";
                    unset($annonce->id_categorie);
                    $annonce->categorie = $categorie;
                    $response['results'] =$annonce;
                }
            }catch(Exception $e){
                $response['status'] = 500;
                $app->response->setBody("");
                $app->response->setStatus(500);
            }
            echo json_encode($response);
        })->name("api/annonces/id");

        $app->post('',function() use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $app->response->setStatus(201);
            $response['status'] = 201;
            try {
                $errors = array();
                $apikey = $app->request->get("API_KEY");
                $post = $app->request->post();
                if ($apikey != null) {
                    $membre = \model\Membre::where("api_key", "=", $apikey)->first();
                    if ($membre===null){
                        throw new \Exception();
                    }
                    if (isset($post['titre']) && isset($post['description']) && isset($post['prix']) && isset($post['categorie'])) {
                        $annonce = new \model\Annonce();
                        $annonce->id_membre = $membre->id;
                        $annonce->date_creation = date('Y-m-d H:i:s');
                        \controller\AnnonceController::sanitize($post,$annonce,$errors);

                        if(sizeof($errors) == 0) {
                            $annonce->save();
                            $categorie = \model\Categorie::find($annonce->id_categorie);
                            $categorie->type = "categorie";
                            $img = $categorie->image;
                            $categorie->image = array("href"=>$img);
                            $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                            $link = array("self"=>$self);
                            $categorie->link = $link;
                            $annonce->type = "annonce";
                            unset($annonce->id_categorie);
                            $annonce->categorie = $categorie;
                            $response['results'] =$annonce;

                        } else {
                            throw new \Exception();
                        }
                    }
                }
            } catch (Exception $e) {
                $app->response->setStatus(400);
                $response['status'] = 400;
            }
            echo json_encode($response);

        })->name("api/annonces/post");

        $app->delete('/:id',function($id) use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;
            try {
                $apikey = $app->request->get("API_KEY");
                if ($apikey != null) {
                    $membre = \model\Membre::where("api_key", "=", $apikey)->first();
                    $annonce = \model\Annonce::find($id);
                    if ($annonce == null) {
                        $app->response->setStatus(404);
                        $response['status'] = 404;
                    }
                    if ($annonce->id_membre == $membre->id){
                        $annonce->delete();
                    }else{
                        $app->response->setStatus(403);
                        $response['status'] = 403;
                    }
                }
            } catch(Exception $e) {
                $response['status'] = 400;
            }
            echo json_encode($response);
        })->name("api/annonces/id/delete");

        $app->patch('/:id',function($id) use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $app->response->setStatus(200);
            $response['status'] = 200;
            try{
                $errors = array();
                $apikey = $app->request->get("API_KEY");
                $post = $app->request->post();
                if ($apikey != null){
                    $membre = \model\Membre::where("api_key", "=", $apikey)->first();
                    if ($membre===null){
                        throw new \Exception();
                    }
                    $annonce = \model\Annonce::find($id);
                    if ($annonce->id_membre != $membre->id){
                        throw new \Exception();
                    }
                    if (isset($post['titre'])){
                        \controller\AnnonceController::sanitizeTitre($post['titre'],$annonce,$errors);
                    }
                    if (isset($post['descriptif'])){
                        \controller\AnnonceController::sanitizeDescription($post['descriptif'],$annonce,$errors);
                    }
                    if (isset($post['prix'])){
                        \controller\AnnonceController::sanitizePrix($post['prix'],$annonce,$errors);
                    }
                    if (isset($post['categorie'])){
                        \controller\AnnonceController::sanitizeCategorie($post['categorie'],$annonce,$errors);
                    }
                    if(sizeof($errors) == 0) {
                        $annonce->save();
                        $categorie = \model\Categorie::find($annonce->id_categorie);
                        $categorie->type = "categorie";
                        $img = $categorie->image;
                        $categorie->image = array("href"=>$img);
                        $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                        $link = array("self"=>$self);
                        $categorie->link = $link;
                        $annonce->type = "annonce";
                        unset($annonce->id_categorie);
                        $annonce->categorie = $categorie;
                        $response['results'] =$annonce;

                    }else{
                        throw new \Exception();
                    }
                }
            } catch (Exception $e) {
                $app->response->setStatus(400);
                $response['status'] = 400;
            }
            echo json_encode($response);
        })->name("api/annonces/id/put");

    });

    $app->group('/categories',function() use ($app){
        $app->get('',function() use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;
            try{
                $categories = \model\Categorie::all();
                if (sizeof($categories) ==0){
                    $app->response->setStatus(500);
                    $response['status'] = 500;
                }else{
                    foreach($categories as $categorie){
                        $categorie->type = "categorie";
                        $img = $categorie->image;
                        $categorie->image = array("href"=>$img);
                        $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                        $link = array("self"=>$self);
                        $categorie->link = $link;
                    }
                    $response['results'] = $categories;
                }
            }catch (Exception $e){
                $response['status'] = 500;
                $app->response->setBody("");
                $app->response->setStatus(500);
            }
            echo json_encode($response);
        })->name("api/categories");

        $app->get('/:id',function($id) use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;
            try{
                $categorie = \model\Categorie::find($id);
                if ($categorie == null){
                    $app->response->setStatus(404);
                    $response['status'] = 404;
                }else{
                    $categorie->type = "categorie";
                    $img = $categorie->image;
                    $categorie->image = array("href"=>$img);
                    $annonces = array("href"=>$app->urlFor("api/categories/id/annonces",array("id"=>$categorie->id)));
                    $link = array("annonces"=>$annonces);
                    $categorie->link = $link;

                    $response['results'] = $categorie;
                }
            }catch (Exception $e){
                $response['status'] = 500;
                $app->response->setBody("");
                $app->response->setStatus(500);
            }
            echo json_encode($response);
        })->name("api/categories/id");

        $app->get('/:id/annonces',function($id) use ($app){

            $app->response->headers->set('Content-Type', 'application/json');
            $response = array();
            $response['status'] = 200;
            try{
                $categorie = \model\Categorie::where("id", "=", $id)->get();
                if($categorie == null){
                    $response['status'] = 404;
                    $app->response->setStatus(404);
                }else{
                    $annonces = \model\Annonce::where('id_categorie', '=', $id)->get();
                    if(sizeof($annonces) == 0){
                        $response['status'] = 404;
                        $app->response->setStatus(404);
                    }else{
                        foreach($annonces as $annonce){
                            $categorie = \model\Categorie::find($annonce->id_categorie);
                            $categorie->type = "categorie";
                            $img = $categorie->image;
                            $categorie->image = array("href"=>$img);
                            $self = array("href"=>$app->urlFor("api/categories/id",array("id"=>$categorie->id)));
                            $link = array("self"=>$self);
                            $categorie->link = $link;

                            $annonce->type = "annonce";
                            unset($annonce->id_categorie);
                            $annonce->categorie = $categorie;
                            $self = array("href"=>$app->urlFor("api/annonces/id",array('id'=>$annonce->id)));
                            $link=array("self"=>$self);
                            $annonce->link = $link;
                        }
                        $response['results'] = $annonces;
                    }
                }
            }catch (Exception $e){
                $response['status'] = 500;
                $app->response->setBody("");
                $app->response->setStatus(500);
            }
            echo json_encode($response);
        })->name("api/categories/id/annonces");


    });
});



function links() {
    $app = \Slim\Slim::getInstance();
    return [
        'indexPath' => $app->urlFor('index'),
        'rechercherPath' => $app->urlFor('recherche'),
        'administrerPath' => $app->urlFor('index'),
        'ajouterAnnoncePath' => $app->urlFor('ajouterAnnonce'),
        'connecterPath' => $app->urlFor('connexion'),
        'connecterPostPath' => $app->urlFor('connexionPost'),
        'inscrirePath' => $app->urlFor('inscription'),
        'inscrirePostPath' => $app->urlFor('inscriptionPost'),
        'deconnecterPath' => $app->urlFor('deconnexion'),
        'mesAnnoncesPath' => $app->urlFor('mesAnnonces'),
        'profilPath' => $app->urlFor('profil'),
        'editerProfilPath' => $app->urlFor('editerProfil'),
        'editerMotDePassePath' => $app->urlFor('editerMotDePasse'),
        'editerMotDePassePostPath' => $app->urlFor('editerMotDePassePost'),
        'genererAPIPath' => $app->urlFor('genererAPI'),
        'genererAPIPostPath' => $app->urlFor('genererAPIPost'),
    ];
}

function generateToken() {
    $tokenBin = openssl_random_pseudo_bytes(20);
    $token = bin2hex($tokenBin);
    $_SESSION['token'] = $token;
    return $token;
}

$app->run();