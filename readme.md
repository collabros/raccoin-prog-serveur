# Projet Programmation Web Serveur - Raccoin

## Membres du groupes 
- Jordan Becker
- Jacques Cornat
- Thomas Frantz
- Julien Michel

##Projet en ligne
- [Serveur Racoin](http://ec2-54-93-103-31.eu-central-1.compute.amazonaws.com/racoin)

##Informations importantes
- Il est **important** de mettre les droits à Apache dans le dossier photos pour qu'il puisse créer des dossiers et fichiers, et en supprimer.
- Faire une copie du fichier config/config.ini.default en config/config.ini, et le remplir avec les identfiants de sa base e données
- Il y a 2 comptes de tests : les identifiants sont "test@test.com", et "test@test2.com", et le mot de passe pour les 2 comptes sont "aaaaa"
- Le fichier SQL contenants quelques annonces et comptes se trouve dans le dossier sql/.
- Le dépôt Git est disponible ici : https://bitbucket.org/collabros/raccoin-prog-serveur/
- Le dashboard du projet est accessible ici : https://docs.google.com/spreadsheets/d/1SyNXHoD02S-uBYqmLayep07kMLSH4ItYaxVE7-T04bY/edit?usp=sharing
- En cliquant sur le nom de l'utilisateur, il est possible d'accéder à une interface pour gérer son compte
- Toute annonce créée, même sans compte génère un compte pour l'utilisateur

## Technologies
- Composer
- [Slim (Micro Framework)](https://packagist.org/packages/slim/slim)
- [Twig (Templating)](https://packagist.org/packages/twig/twig)
- [ORM : Eloquent (DB)](https://packagist.org/packages/illuminate/database)
- [Foundation SASS (Design)](foundation.zurb.com)