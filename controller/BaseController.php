<?php
namespace controller;

    use Slim\Http\Request;
    use Slim\Slim;

    abstract class BaseController{
	
    public $request; // HttpRequest

    function __construct(Request $request) {
        $this->request = $request;
        $this->app = Slim::getInstance();
    }
}