<?php
namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\Annonce;
use model\Categorie;
use model\Membre;
use view\AjouterAnnonce;
use view\Detail;
use view\ListeAnnonces;
use view\PrevisualiserAnnonce;
use view\Recherche;

class AnnonceController extends BaseController {

    public function recherche() {

        $titre = $this->app->request->get('q');
        $min = $this->app->request->get('min');
        $max = $this->app->request->get('max');
        $categorie_select = $this->app->request->get('categorie');
        $departement_select = $this->app->request->get('departement');
        $querybuilder = Annonce::with('member');

        if($titre != null){
            $querybuilder = $querybuilder->where('titre','like','%'.$titre.'%');
        }
        if ($max != null){
            $querybuilder = $querybuilder->where('prix','<=',$max);
        }
        if($min != null){
            $querybuilder = $querybuilder->where('prix','>=',$min);
        }
        if($categorie_select != null && $categorie_select != 0){
            $querybuilder = $querybuilder->where('id_categorie','=',$categorie_select);
        }
        if($departement_select != null && $departement_select != 1){
//        $querybuilder = $querybuilder->where('membre','member.code_postal','like',$departement_select.'%');
        }

        $annonces = $querybuilder->orderBy('date_creation', 'desc')->get();

        foreach($annonces as $annonce){
            $annonce['url'] = $this->app->urlFor('annonce',array('id' => $annonce['id']) );
            $annonce['image'] = $annonce->cover();
        }

        $view = new Recherche();

        if (sizeof($annonces) == 0) {
            $view->addVar('no_results',"Aucun résultat à votre recherche.");
        }

        $categories = Categorie::all();
        $view->addVar('categories', $categories);
        $view->addVar('link',links());
        $view->addVar('titreQ', $titre);
        $view->addVar('minQ', $min);
        $view->addVar('maxQ', $max);
        $view->addVar('categorieQ', $categorie_select);
        $view->addVar('departementQ', $departement_select);
        $view->addVar('annonces', $annonces);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

    public function detail($id){

        try {
            $annonce = Annonce::findOrFail($id);
        } catch(ModelNotFoundException $e) {
            $this->app->error("Annonce non disponible");
        }

        $token = generateToken();

        $categorie = Categorie::find($annonce['id_categorie']);
        $categorie['url'] = $this->app->urlFor('categorie',array('id' => $categorie['id']) );
        $membre = Membre::find($annonce['id_membre']);
        $images= $annonce->images;
        $view = new Detail();
        $view->addVar('images', $images);
        $view->addVar('categorie', $categorie);
        $view->addVar('annonce', $annonce);
        $view->addVar('token', $token);
        $view->addVar('membre', $membre);
        $view->addVar('editerAnnonce', $this->app->urlFor('editerAnnonce', array('id' => $annonce['id'])));

        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function liste() {
        $annonces = Annonce::with('images')->get();

        if (sizeof($annonces) == 0) {
            $this->app->error("Aucune annonce pour cette catégorie");
        }
        foreach ($annonces as $annonce) {
            $annonce['url'] = $this->app->urlFor('annonce',array('id' => $annonce['id']) );
            $annonce['image'] = $annonce->cover();
        }

        $view = new ListeAnnonces();
        $view->addVar('link',links());
        $view->addVar('annonces', $annonces);
        $view->addVar('url', $annonces);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

    public function edition($id) {

        try {
            $annonce = Annonce::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Annonce non disponible");
        }
        try {
            $membre = Membre::findOrFail($annonce['id_membre']);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Membre non disponible");
        }
        try {
            $categorie = Categorie::findOrFail($annonce['id_categorie']);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Catégorie non disponible");
        }


        $array = array(
            'annonce' => $annonce,
            'membre' => $membre,
            'edition' => true,
            'categorie' => $categorie,
            'postroute' => $this->app->urlFor('editerAnnoncePost', array('id' => $id)),
            'titre' => 'Modifier une annonce',
            'supprimer' => $this->app->urlFor('supprimerAnnonce',array('id' => $id))
        );

        $images = $annonce->images()->get();
        foreach($images as $key => $image) {
            $array['images'][$key] = $image;
        }

        $this->ajouter($array);
    }

    public function editionPost($id) {


        try {
            $annonce = Annonce::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Annonce non disponible");
        }
        try {
            $membre = Membre::findOrFail($annonce['id_membre']);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Membre non disponible");
        }


        $pass = $this->app->request->post('mot_de_passe');
        $mdp = $membre->mot_passe;
        if (!password_verify($pass, $mdp)) {
            $this->app->flash('error', 'Mot de passe incorrect');
            $this->app->redirect($this->app->urlFor('editerAnnonce', array('id' => $id)));
        }


        try {
            Categorie::findOrFail($annonce['id_categorie']);
        } catch (ModelNotFoundException $e) {
            $this->app->error("Catégorie non disponible");
        }

        $this->ajouterP($annonce, $membre, array('postroute' => $this->app->urlFor('editerAnnoncePost', array('id' => $id))));

    }

    public function ajouter($params = null) {
        $view = new AjouterAnnonce();
        $categories = Categorie::all();
        $view->addVar('categories', $categories);
        $view->addVar('titre', "Ajouter une annonce");

        $view->addVar('link',links());

        $token = generateToken();
        $view->addVar('token', $token);

        $view->addVar('postroute', $this->app->urlFor('ajouterAnnoncePost'));
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);

        if(is_array($params)) {
            foreach ($params as $key => $param) {
                $view->addVar($key, $param);
            }
        }

        echo $view->render();
    }

    public function ajouterP($annonce, $membre, $params) {

        $res = $this->app->request->post();

        if(!isset($_SESSION['id'])) {
            //Si on est dans la phase de retour à l'édition après visualisation
            if(array_key_exists('back', $res)) {
                //Petit trick juste pour que la sanitisation du membre fonctionne
                $res['mot_de_passe'] = "aaaaa";
            }
            MembreController::sanitize($res, $membre, $errors);
        } else {
            MembreController::membreDepuisSession($membre);
        }
        AnnonceController::sanitize($res, $annonce, $errors);

        //S'il y a des erreurs de sanitisation, on remplit son formulaire avec les données valides
        if(sizeof($errors) > 0) {
            $array = array('errors' => $errors, 'membre' => $membre, 'annonce' => $annonce);
            $this->ajouter($array);
            exit;
        }

        if($_SESSION['token'] != $_POST['token']) {
            $errors["Token"] = "Clé d'identification invalide";
            $array = array('errors' => $errors, 'membre' => $membre, 'annonce' => $annonce);
            $this->ajouter($array);
            exit;
        }

        //Si l'utilisateur revient de la prévisualisation pour éditer son annonce
        if (array_key_exists('back', $res)) {
            $array = array('errors' => $errors, 'membre' => $membre, 'annonce' => $annonce);
            $this->ajouter($array);
            exit;
        }

        //Si l'utilisateur veut prévisualiser
        if(array_key_exists('prev', $res)) {
            if(isset($_SESSION['id'])) {
                MembreController::membreDepuisSession($membre);
            } else {
                MembreController::sanitize($res, $membre, $errors);
            }

            $annonce->date_creation = date('Y-m-d H:i:s');
            AnnonceController::sanitize($res, $annonce, $errors);

            if(sizeof($errors) == 0) {
                $imagesArr=array();
                $count=1;
                foreach ($_FILES as $name => $file) {
                    if ($file['error'] == 0) {
                        $imagesArr[$count]["data"]=ImageController::prevPhotos($name, $file, $errors);
                        $imagesArr[$count]["position"]=$count;
                        $count++;
                    }
                }
            }

            $view = new Detail();
            $view->addVar('link',links());
            $token = generateToken();
            $view->addVar('token',$token);
            $view->addVar('annonce',$annonce);
            $view->addVar('membre',$membre);
            $view->addVar('previsualisation',true);
            $view->addVar('postroute',$this->app->urlFor('ajouterAnnoncePost'));
            $view->addVar('images', $imagesArr);
            if(is_array($params)) {
                foreach ($params as $key => $param) {
                    $view->addVar($key, $param);
                }
            }

            $env = $this->app->environment();
            $view->addVar('path',$env['SCRIPT_NAME']);
            $view->addVar('session', $_SESSION);
            echo $view->render();
            exit;
        }

        //Sinon on ajoute l'annonce en base

        if(isset($_SESSION['id'])) {
            $annonce->id_membre = $_SESSION['id'];
        } else {
            $membre->mot_passe = password_hash($res['mot_de_passe'], PASSWORD_DEFAULT, array('cost'=> 12));
            $membre->save();
            if(isset($membre->id)) {
                $annonce->id_membre = $membre->id;
            }
        }

        $annonce->date_creation = date('Y-m-d H:i:s');
        $annonce->save();
        foreach ($_FILES as $name => $file) {
            ImageController::deletePhoto($name, $annonce->id);
            ImageController::sanitizePhotos($name, $file, $annonce['id'], $errors);
        }

        $this->app->flash('success', 'Annonce modifiée avec succès');
        $this->app->redirect($this->app->urlFor('annonce', array('id' => $annonce->id)));
    }

    public function ajouterPost(){

        $errors = array();
        $membre = new Membre();
        $annonce = new Annonce();

        $this->ajouterP($annonce, $membre, array('postroute' => $this->app->urlFor('ajouterAnnoncePost')));

    }

    public function supprimer($id){

        if($_SESSION['token'] != $_POST['token']) {
            $this->app->flash('error', 'Clé d\'identification invalide');
            if(isset($_POST['id']) && is_numeric($_POST['id'])) {
                $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'connexion', 'param' => array('id', $_POST['id'])));
            } else {
                $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'index'));
            }
        }

        try {
            $annonce = Annonce::find($id);
            $membre = Membre::find($annonce["id_membre"]);
        } catch (\Exception $e) {
            $this->app->redirect($this->app->urlFor('annonces'));
        }

        $pass = $this->app->request->post('passwordSupprimer');
        $mdp = $membre->mot_passe;
        if (!password_verify($pass, $mdp)) {
            $this->app->flash('error', 'Mot de passe incorrect');
            $this->app->redirect($this->app->urlFor('editerAnnonce', array('id'=>$id)));
        }
        ImageController::deletePhotos($id);
        $annonce->delete();
        $this->app->flash('success', 'Annonce supprimée avec succès');
        $this->app->redirect($this->app->urlFor('index'));
    }

    //Fonction utiliser pour nettoyer et définir les données en entrée d'un ajout ou d'un update d'annonce
    public static function sanitize($data, $annonce, &$errors) {

        if(array_key_exists('titre',$data)){
            AnnonceController::sanitizeTitre($data['titre'],$annonce,$errors);
        }else{
            $errors['titre'] = "Champ titre non défini";
        }

        if(array_key_exists('description',$data)){
            AnnonceController::sanitizeDescription($data['description'],$annonce,$errors);
        }else{
            $errors['description'] = "Champ description non défini";
        }

        if(array_key_exists('prix',$data)){
            AnnonceController::sanitizePrix($data['prix'],$annonce,$errors);
        }else{
            $errors['prix'] = "Champ prix non défini";
        }

        if(array_key_exists('categorie',$data)){
            AnnonceController::sanitizeCategorie($data['categorie'],$annonce,$errors);
        }else{
            $errors['categorie'] = "Champ categorie non défini";
        }

        //Test if category exists
        try {
            if (is_numeric($data['categorie'])) {
                $categorie = Categorie::where('id', $data['categorie'])->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            $errors['categorie'] = "Aucune catégorie trouvée";
        }
        if(!isset($categorie)) {
            $errors['categorie'] = "Erreur sur la catégorie (catégorie non trouvée)";
        } else {
            $annonce->id_categorie = $categorie->id;
        }
    }

    public static function sanitizeTitre($value,$annonce,&$errors){
        if(strlen($value) < 4 || strlen($value) > 50) {
            $errors['titre'] = "Erreur sur le titre (entre 4 et 50 caractères)";
        } else {
            $annonce->titre = $value;
        }
    }

    public static function sanitizeDescription($value,$annonce,&$errors){
        if(strlen($value) < 10 || strlen($value) > 500) {
            $errors['description'] = "Erreur sur la description (entre 10 et 100 caractères)";
        } else {
            $annonce->descriptif = $value;
        }
    }

    public static function sanitizePrix($value,$annonce,&$errors){
        if(!is_numeric($value) || strlen($value) > 10000000) {
            $errors['prix'] = "Erreur sur le prix (seuls chiffres acceptés)";
        } else {
            $annonce->prix = $value;
        }
    }

    public static function sanitizeCategorie($value,$annonce,&$errors){
        try {
            if (is_numeric($value)) {
                $categorie = Categorie::where('id', $value)->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            $errors['categorie'] = "Aucune catégorie trouvée";
        }
        if(!isset($categorie)) {
            $errors['categorie'] = "Erreur sur la catégorie (catégorie non trouvée)";
        } else {
            $annonce->id_categorie = $categorie->id;
        }
    }

}