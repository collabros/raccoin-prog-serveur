<?php
namespace controller;

class ImageController extends BaseController {

    /**
     * @param $name String Nom de l'input côté HTML (exemple: photo1, photo2, ...)
     * @param $file Array Tableau contenant les informations du fichier ('name', 'type', 'tmp_name', 'error', 'size')
     * @param $idannonce Int Id de l'annonce
     * @param $errors Array Tableau d'erreurs à remonter
     */
    public static function sanitizePhotos($name, $file, $idannonce, &$errors) {
        $uploaddir = "photos/annonce-$idannonce/";
        $position=substr($name, -1);

        if ($file['error'] == 0 && is_numeric($position) && $position <= 3 && $position >= 1) {

            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0755, true);
            }
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            $uploadfile = $uploaddir . $name . "." . $extension;

            if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
                //Enregistrer dans la DB
                $img=new \model\Image();
                $img->id_annonce=$idannonce;
                $img->url=$uploadfile;
                $img->position=$position;
                $img->save();
            }
            else {
                $errors[]="Problème d'upload de ".$file['name'];
            }
        }
        else {
            if ($file['error'] != 4) {
                $errors[]="Problème d'upload de ".$file['name'];
            }
        }
    }

    /**
     * Supprimme toutes les photos d'une annonce
     * @param $idannonce int
     */
    public static function deletePhotos($idannonce) {
        $uploaddir = "photos/annonce-$idannonce/";

        $imgs=\model\Image::where('id_annonce', $idannonce)->get();
        foreach ($imgs as $img) {
            if (file_exists($img["url"])) {
                unlink($img["url"]);
                $img->delete();
            }

        }

        if (file_exists($uploaddir)) {
            rmdir($uploaddir);
        }
    }

    /**
     * Supprimme une photo d'une annonce
     * @param $position int/String Peut être sous la forme "photo1"
     * @param $idannonce int
     */
    public static function deletePhoto($position, $idannonce) {
        $position=substr($position, -1);

        $img=\model\Image::where('position', $position)->where('id_annonce', $idannonce)->first();
        if ($img != null) {
            try {
                unlink($img['url']);
            } catch (\ErrorException $e) {

            }
            $img->delete();
        }
    }

    /**
     * Retourne une URL de fichier image temporaire
     * @param $name String Nom de l'input côté HTML (exemple: photo1, photo2, ...)
     * @param $file Array Tableau contenant les informations du fichier ('name', 'type', 'tmp_name', 'error', 'size')
     * @param $errors Array Tableau d'erreurs à remonter
     */
    public static function prevPhotos($name, $file, &$errors) {
        $position=substr($name, -1);
        $prefix="data:".$file['type'].";base64,";

        if ($file['error'] == 0 && is_numeric($position) && $position <= 3 && $position >= 1) {
            return $prefix.base64_encode(file_get_contents($file['tmp_name']));
        }
        else {
            if ($file['error'] != 4) {
                $errors[]="Problème d'upload de ".$file['name'];
            }
        }
    }

}