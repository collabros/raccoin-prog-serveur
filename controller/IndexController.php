<?php
namespace controller;

use model\Categorie;
use view\Home;

class IndexController extends BaseController {

    public function index() {

        $categories = Categorie::all();

        foreach ($categories as $categorie) {
            $categorie['url'] = $this->app->urlFor('categorie',array('id' => $categorie['id']) );
        }

        $view = new Home();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('categories', $categories);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

}