/*
 Navicat Premium Data Transfer

 Source Server         : jb.dev
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : localhost
 Source Database       : Racoin

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : utf-8

 Date: 03/06/2015 00:54:36 AM
*/

CREATE DATABASE  IF NOT EXISTS `Racoin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `Racoin`;

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `annonce`
-- ----------------------------
DROP TABLE IF EXISTS `annonce`;
CREATE TABLE `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) DEFAULT NULL,
  `descriptif` varchar(1024) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `id_membre` int(11) DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_annonce_1_idx` (`id_membre`),
  KEY `fk_annonce_2_idx` (`id_categorie`),
  CONSTRAINT `fk_annonce_1` FOREIGN KEY (`id_membre`) REFERENCES `membre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_annonce_2` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `annonce`
-- ----------------------------
BEGIN;
INSERT INTO `annonce` VALUES ('1', 'MacBook Pro Retina 15\"', 'Ordinateur Apple en très bon état, quelques traces d\'utilisation normale. Il n\'a servi que quelques mois, et je m\'en sépare au profit de l\'acquisition d\'un Mac Pro. Stockage flash 512GB, 8GB RAM, Intel Core i7 2,6GHz', '1990', '1', '2015-03-06 00:37:39', '3', null), ('2', 'Voiture biseautée aérodynamique', 'Je vends donc ma première voiture, il est temps de passer à un modèle plus sérieux après mes premières années de permis. Un petit accrochage sur l\'aile gauche, du à un poteau de signalisation mal placé.', '5000', '1', '2015-03-06 00:43:27', '1', null), ('3', 'Pebble Watch Edition Limitée', 'Exclusivement sur Racoin, ma Pebble Watch en Kickstarter Edition. C\'est une occasion en or, la montre n\'a été portée que pour faire des tests et des photos pour un site de news.', '150', '1', '2015-03-06 00:50:56', '5', null);
COMMIT;

-- ----------------------------
--  Table structure for `annonce_tag`
-- ----------------------------
DROP TABLE IF EXISTS `annonce_tag`;
CREATE TABLE `annonce_tag` (
  `id_annonce` int(11) DEFAULT NULL,
  `id_tag` int(11) DEFAULT NULL,
  KEY `fk_annonce_tag_2_idx` (`id_tag`),
  KEY `fk_annonce_tag_1` (`id_annonce`),
  CONSTRAINT `fk_annonce_tag_1` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_annonce_tag_2` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `categorie`
-- ----------------------------
DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `categorie`
-- ----------------------------
BEGIN;
INSERT INTO `categorie` VALUES ('1', 'Véhicule', 'img/categories/vehicule.jpg'), ('2', 'Immobilier', 'img/categories/immobilier.jpg'), ('3', 'Multimédia', 'img/categories/multimedia.jpg'), ('4', 'Vêtements', 'img/categories/vetements.jpg'), ('5', 'Montre', 'img/categories/montre.jpg'), ('6', 'Bijoux', 'img/categories/bijoux.jpg'), ('7', 'Parfum', 'img/categories/parfum.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `image`
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_annonce` int(11) DEFAULT NULL,
  `position` int(1) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_1_idx` (`id_annonce`),
  CONSTRAINT `fk_images_1` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `image`
-- ----------------------------
BEGIN;
INSERT INTO `image` VALUES ('1', '1', '1', 'photos/annonce-1/photo1.jpg'), ('2', '1', '2', 'photos/annonce-1/photo2.jpg'), ('3', '1', '3', 'photos/annonce-1/photo3.jpg'), ('4', '2', '1', 'photos/annonce-2/photo1.jpg'), ('5', '2', '2', 'photos/annonce-2/photo2.jpg'), ('6', '3', '1', 'photos/annonce-3/photo1.jpg'), ('7', '3', '2', 'photos/annonce-3/photo2.jpg'), ('8', '3', '3', 'photos/annonce-3/photo3.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `information_pro`
-- ----------------------------
DROP TABLE IF EXISTS `information_pro`;
CREATE TABLE `information_pro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `siret` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `membre`
-- ----------------------------
DROP TABLE IF EXISTS `membre`;
CREATE TABLE `membre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `mot_passe` varchar(256) DEFAULT NULL,
  `enregistre` varchar(1) DEFAULT NULL,
  `code_postal` varchar(5) DEFAULT NULL,
  `api_key` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `membre`
-- ----------------------------
BEGIN;
INSERT INTO `membre` VALUES ('1', 'Becker', 'Jordan', 'jordanbecker@domain.tld', '06123456789', '$2y$12$96K4G9LKiBPor3YgYMmBme4qG6XWb1WBxdPoWvHEr.GiaQFi0HYZW', null, '54000', null);
COMMIT;

-- ----------------------------
--  Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
