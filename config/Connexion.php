<?php
namespace config;
use Illuminate\Database\Capsule\Manager as DB;

class Connexion {

    public static function connection($filename) {
        $db = new DB();
        $ini_array = parse_ini_file($filename);

        if(!$ini_array) throw new \Exception("INI FILE $filename CANNOT BE LOADED");

        $db->addConnection($ini_array);
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}
