<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    public $table = 'image';
    public $idTable = 'id';
    public $timestamps = false;
//
//    public function ads() {
//        return $this->belongsToMany('Image', 'annonce_tag', 'id_annonce', 'id_tag');
//    }

    public function annonce() {
        return $this->belongsTo('model\Annonce', 'id_annonce');
    }

}