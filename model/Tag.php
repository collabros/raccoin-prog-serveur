<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model{

    public $table = 'tag';
    public $idTable = 'id';
    public $timestamps = false;

    public function ads() {
        return $this->belongsToMany('Annonce', 'annonce_tag', 'id_annonce', 'id_tag');
    }

}