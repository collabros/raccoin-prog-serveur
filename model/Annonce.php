<?php namespace model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Annonce extends Model{

    use SoftDeletingTrait;

    public $table = 'annonce';
    public $idTable = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function member() {
        return $this->belongsTo('model\Membre', 'id_membre');
    }

    public function category() {
        return $this->belongsTo('model\Categorie', 'id_categorie');
    }

    public function tags() {
        return $this->belongsToMany('model\Tag', 'annonce_tag', 'id_annonce', 'id_tag');
    }

    public function images() {
        return $this->hasMany('model\Image', 'id_annonce');
    }

    public function cover() {
        $image = Image::where('id_annonce', '=', $this->id)->where('position', '=', '1')->first();
        return $image;
    }
}