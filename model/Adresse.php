<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Adresse extends Model{

    public $table = 'adresse';
    public $idTable = 'id';
    public $timestamps = false;

    public function members() {
        return $this->hasMany('Membre', 'id_adresse');
    }

    public function department() {
        return $this->belongsTo('Departement', 'id_departement');
    }
}